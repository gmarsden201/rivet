Name: LHCB_2015_I1401225
Year: 2015
Summary: Mass distributions in the decay $D^0\to K^-\pi^+\mu^+\mu^-$
Experiment: LHCB
Collider: LHC
InspireID: 1401225
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 757 (2016) 558-567
RunInfo: Any process producing D0
Description:
  'Kinematic distributions  in the decay $D^0\to K^-\pi^+e^+e^-$. Resolution/acceptance effects have been not unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2015yuk
BibTeX: '@article{LHCb:2015yuk,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{First observation of the decay $D^{0}\rightarrow K^{-}\pi^{+}\mu^{+}\mu^{-}$ in the $\rho^{0}$-$\omega$ region of the dimuon mass spectrum}",
    eprint = "1510.08367",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "LHCB-PAPER-2015-043, CERN-PH-EP-2015-283",
    doi = "10.1016/j.physletb.2016.04.029",
    journal = "Phys. Lett. B",
    volume = "757",
    pages = "558--567",
    year = "2016"
}
'
