BEGIN PLOT /ATLAS_2023_I2663256/d..
LegendAlign=r
XTwosidedTicks=1
YTwosidedTicks=1
LogY=1
END PLOT

BEGIN PLOT /ATLAS_2023_I2663256/d01-x01-y08
YLabel=Events
XCustomMajorTicks=1.5  Bin1  2.5  Bin2  3.5  Bin3 4.5  Bin4  5.5  Bin5  6.5  Bin6  7.5  Bin7  8.5  Bin8  9.5  Bin9
YMin=1
YMax=10E8
END PLOT


BEGIN PLOT /ATLAS_2023_I2663256/d02-x01-y08
YLabel=Events/100 GeV
XLabel= $H_T$ [GeV]
YMin=1
YMax=10E7
END PLOT

BEGIN PLOT /ATLAS_2023_I2663256/d03-x01-y08
YLabel=Events/50 GeV
XLabel= $E_T^{miss}$ [GeV]
YMin=1
YMax=10E7
END PLOT

BEGIN PLOT /ATLAS_2023_I2663256/d04-x01-y08
YLabel=Events/0.05 
XLabel= $p_T^{bal}$ 
YMin=1
YMax=10E8
END PLOT


BEGIN PLOT /ATLAS_2023_I2663256/d05-x01-y08
YLabel=Events/0.14
XLabel= $|\phi_{max} -\phi_{min}|$ [GeV]
YMin=1
YMax=10E8
END PLOT