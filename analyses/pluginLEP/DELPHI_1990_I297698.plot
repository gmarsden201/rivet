BEGIN PLOT /DELPHI_1990_I297698/d01-x01-y01
Title=Integrated 2-jet rate with Jade algorithm
XLabel=$y_{\text{cut}}^\text{Jade}$
YLabel=$R_2$ [$\%$]
LogX=1
END PLOT
BEGIN PLOT /DELPHI_1990_I297698/d01-x01-y02
Title=Integrated 3-jet rate with Jade algorithm
XLabel=$y_{\text{cut}}^\text{Jade}$
YLabel=$R_3$ [$\%$]
LogX=1
END PLOT
BEGIN PLOT /DELPHI_1990_I297698/d01-x01-y03
Title=Integrated 4-jet rate with Jade algorithm
XLabel=$y_{\text{cut}}^\text{Jade}$
YLabel=$R_4$ [$\%$]
LogX=1
END PLOT
BEGIN PLOT /DELPHI_1990_I297698/d01-x01-y04
Title=Integrated 5-jet rate with Jade algorithm
XLabel=$y_{\text{cut}}^\text{Jade}$
YLabel=$R_5$ [$\%$]
LogX=1
END PLOT
