BEGIN PLOT /BABAR_2021_I1844422/
LogY=0
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
END PLOT
BEGIN PLOT /BABAR_2021_I1844422/d01-x01-y01
Title=Cross section for $e^+e^-\to 2\pi^+2\pi^-3\pi^0$
END PLOT
BEGIN PLOT /BABAR_2021_I1844422/d02-x01-y01
Title=Cross section for $e^+e^-\to 2\pi^+2\pi^-\eta$
END PLOT
BEGIN PLOT /BABAR_2021_I1844422/d03-x01-y01
Title=Cross section for $e^+e^-\to \omega\pi^0\eta$
END PLOT
BEGIN PLOT /BABAR_2021_I1844422/d04-x01-y01
Title=Cross section for $e^+e^-\to \pi^+\pi^-2\pi^0\omega$
END PLOT
BEGIN PLOT /BABAR_2021_I1844422/d05-x01-y01
Title=Cross section for $e^+e^-\to \pi^+\pi^-2\pi^0\eta$
END PLOT
BEGIN PLOT /BABAR_2021_I1844422/d06-x01-y01
Title=Cross section for $e^+e^-\to 2\pi^+2\pi^-2\pi^0\eta$
END PLOT

