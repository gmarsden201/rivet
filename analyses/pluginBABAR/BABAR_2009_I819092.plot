BEGIN PLOT /BABAR_2009_I819092/d01-x01-y01
Title=$p\bar\Lambda^0$ mass distribution in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$m_{p\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I819092/d01-x01-y02
Title=$\Lambda^0$ energy distribution in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$E^*_{\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}E^*_{\bar\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2009_I819092/d02-x01-y01
Title=Longitudinal $\Lambda^0$ polarization times $\alpha_{\bar\Lambda^0}$ in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$E^*_{\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$\alpha_{\bar\Lambda^0} P_L$
LogY=0
END PLOT

BEGIN PLOT /BABAR_2009_I819092/d03-x01-y01
Title=Longitudinal $\Lambda^0$ polarization in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$E^*_{\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$P_L$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I819092/d03-x01-y02
Title=Transverse $\Lambda^0$ polarization in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$E^*_{\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$P_T$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I819092/d03-x01-y03
Title=$P_N$ in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$E^*_{\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$P_N$
LogY=0
END PLOT
