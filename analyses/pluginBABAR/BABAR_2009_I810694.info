Name: BABAR_2009_I810694
Year: 2009
Summary: Mass distributions in $B^-\to  D^+\pi^-\pi^-$
Experiment: BABAR
Collider: PEP-II
InspireID: 810694
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 79 (2009) 112004
RunInfo: Any process producing B- mesons, originally Upsilon(4S) decays
Description:
  'Measurement of mass and angular distributions in $B^-\to  D^+\pi^-\pi^-$. The data were read from the plots in the paper and may not be corrected for acceptance/efficiency, however the backgrounds given in the paper were subtracted.'
ValidationInfo:
  'Herwig7 event using EvtGen for decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BaBar:2009pnd
BibTeX: '@article{BaBar:2009pnd,
    author = "Aubert, Bernard and others",
    collaboration = "BaBar",
    title = "{Dalitz Plot Analysis of B- ---\ensuremath{>} D+ pi- pi-}",
    eprint = "0901.1291",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BABAR-PUB-08-048, SLAC-PUB-13508",
    doi = "10.1103/PhysRevD.79.112004",
    journal = "Phys. Rev. D",
    volume = "79",
    pages = "112004",
    year = "2009"
}
'
