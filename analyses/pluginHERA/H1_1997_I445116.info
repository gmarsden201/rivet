Name: H1_1997_I445116
Year: 1997
Summary: Evolution of e p fragmentation and multiplicity distributions in the Breit frame
Experiment: H1
Collider: HERA
InspireID: 445116
Status: VALIDATED
Reentrant: True
Authors:
 - Kritsanon Koennonkok <Kritsanon.k@kkumail.com>
 - Hannes Jung <hannes.jung@desy.de>
References:
 - 'Nucl.Phys.B 504 (1997) 3'
 - 'DOI:10.1016/S0550-3213(97)00585-3'
 - 'arXiv:hep-ex/9707005'
 - 'DESY-97-108'
RunInfo: We use 1,000,000 events with Q2 > 2 for validation.
Beams: [[e+, p+],[p+,e+]]
Energies: [[27.5,820],[820,27.5]]
Description:
  'Low x deep-inelastic ep scattering data, taken in 1994 at the H1 detector at HERA, are analysed in the Breit frame of reference. The evolution of the peak and width of the current hemisphere fragmentation function is presented as a function of Q.'
ValidationInfo:
  'We compare to those from RivetHZTool'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: H1:1997mpq
BibTeX: '@article{H1:1997mpq,
    author = "Adloff, C. and others",
    collaboration = "H1",
    title = "{Evolution of e p fragmentation and multiplicity distributions in the Breit frame}",
    eprint = "hep-ex/9707005",
    archivePrefix = "arXiv",
    reportNumber = "DESY-97-108",
    doi = "10.1016/S0550-3213(97)00585-3",
    journal = "Nucl. Phys. B",
    volume = "504",
    pages = "3--23",
    year = "1997"
}'


