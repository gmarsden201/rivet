BEGIN PLOT /CLEO_2012_I1094160/d01-x01-y03
Title=$K^-\pi^+$ mass distribution in $D^0\to K^0_SK^-\pi^+$ (CLEOIII)
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d01-x01-y02
Title=$K^0_S\pi^+$ mass distribution in $D^0\to K^0_SK^-\pi^+$ (CLEOIII)
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d01-x01-y01
Title=$K^0_SK^-$ mass distribution in $D^0\to K^0_SK^-\pi^+$ (CLEOIII)
XLabel=$m^2_{K^0_SK^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d01-x01-y06
Title=$K^-\pi^+$ mass distribution in $D^0\to K^0_SK^-\pi^+$ (CLEO-c)
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d01-x01-y05
Title=$K^0_S\pi^+$ mass distribution in $D^0\to K^0_SK^-\pi^+$ (CLEO-c)
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d01-x01-y04
Title=$K^0_SK^-$ mass distribution in $D^0\to K^0_SK^-\pi^+$ (CLEO-c)
XLabel=$m^2_{K^0_SK^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/dalitz_1
Title=Dalitz plot for $D^0\to K^0_SK^-\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{K^0_S\pi^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /CLEO_2012_I1094160/d02-x01-y03
Title=$K^+\pi^-$ mass distribution in $D^0\to K^0_SK^+\pi^-$ (CLEOIII)
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d02-x01-y02
Title=$K^0_S\pi^-$ mass distribution in $D^0\to K^0_SK^+\pi^-$ (CLEOIII)
XLabel=$m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d02-x01-y01
Title=$K^0_SK^+$ mass distribution in $D^0\to K^0_SK^+\pi^-$ (CLEOIII)
XLabel=$m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d02-x01-y06
Title=$K^+\pi^-$ mass distribution in $D^0\to K^0_SK^+\pi^-$ (CLEO-c)
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d02-x01-y05
Title=$K^0_S\pi^-$ mass distribution in $D^0\to K^0_SK^+\pi^-$ (CLEO-c)
XLabel=$m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/d02-x01-y04
Title=$K^0_SK^+$ mass distribution in $D^0\to K^0_SK^+\pi^-$ (CLEO-c)
XLabel=$m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2012_I1094160/dalitz_2
Title=Dalitz plot for $D^0\to K^0_SK^+\pi^-$
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^-}/{\rm d}m^2_{K^0_S\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
