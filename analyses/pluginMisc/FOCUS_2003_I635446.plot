BEGIN PLOT /FOCUS_2003_I635446/d01-x01-y01
Title=Lower $\pi^+\pi^-$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2003_I635446/d01-x01-y02
Title=Upper $\pi^+\pi^-$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2003_I635446/dalitz1
Title=Dalitz plot for $D_s^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{{\rm low}\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{{\rm high}\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{{\rm low}\pi^+\pi^-}/{\rm d}m^2_{{\rm high}\pi^+\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2003_I635446/d02-x01-y01
Title=Lower $\pi^+\pi^-$ mass distribution in $D^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2003_I635446/d02-x01-y02
Title=Upper $\pi^+\pi^-$ mass distribution in $D^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2003_I635446/dalitz2
Title=Dalitz plot for $D^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{{\rm low}\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{{\rm high}\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{{\rm low}\pi^+\pi^-}/{\rm d}m^2_{{\rm high}\pi^+\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
