Name: BESIII_2022_I2618227
Year: 2022
Summary: $D_s^+\to\pi^+\pi^+\pi^-+X$ decays
Experiment: BESIII
Collider: BEPC
InspireID: 2618227
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2212.13072 [hep-ex]
RunInfo: Needs e+e- events at 4.178 due cuts
Beams: [e+, e-]
Energies: [4.178]
Description:
'Measurement of mass and momentum distributions in  $D_s^+\to\pi^+\pi^+\pi^-+X$ decays. The mass distribution was read from table VII
 in the paper and is corrected while the momentum distributions were extracted from the figures and are background subtracted but not corrected.'
ValidationInfo:
  'Herwig7 events using EvtGen for decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022ydh
BibTeX: '@article{BESIII:2022ydh,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of the absolute branching fraction of the inclusive decay $D_s^+\to \pi^+\pi^+\pi^- X$}",
    eprint = "2212.13072",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "12",
    year = "2022"
}
'
