BEGIN PLOT /BESIII_2010_I878544/d01-x01-y01
Title=$\pi^0\pi^0$ mass distribution in $\chi_{c0}\to 4\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2010_I878544/d01-x01-y02
Title=$\pi^0\pi^0$ mass distribution in $\chi_{c1}\to 4\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2010_I878544/d01-x01-y03
Title=$\pi^0\pi^0$ mass distribution in $\chi_{c2}\to 4\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
