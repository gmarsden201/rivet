BEGIN PLOT /BESIII_2022_I2166759/d01-x01-y01
Title=$\eta\Sigma^+$ mass distribution in $J/\psi\to \eta\Sigma^+\bar\Sigma^-$
XLabel=$m_{\eta\Sigma^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\Sigma^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2166759/d01-x01-y02
Title=$\eta\bar\Sigma^-$ mass distribution in $J/\psi\to \eta\Sigma^+\bar\Sigma^-$
XLabel=$m_{\eta\bar\Sigma^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\bar\Sigma^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2166759/d01-x01-y03
Title=$\Sigma^+\bar\Sigma^-$ mass distribution in $J/\psi\to \eta\Sigma^+\bar\Sigma^-$
XLabel=$m_{\Sigma^+\bar\Sigma^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^+\bar\Sigma^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2166759/d01-x01-y04
Title=$\eta\Sigma^+$ mass distribution in $\psi(2S)\to \eta\Sigma^+\bar\Sigma^-$
XLabel=$m_{\eta\Sigma^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\Sigma^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2166759/d01-x01-y05
Title=$\eta\bar\Sigma^-$ mass distribution in $\psi(2S)\to \eta\Sigma^+\bar\Sigma^-$
XLabel=$m_{\eta\bar\Sigma^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\bar\Sigma^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2166759/d01-x01-y06
Title=$\Sigma^+\bar\Sigma^-$ mass distribution in $\psi(2S)\to \eta\Sigma^+\bar\Sigma^-$
XLabel=$m_{\Sigma^+\bar\Sigma^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^+\bar\Sigma^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
