BEGIN PLOT /CMS_2021_I1978840/d01-x01-y01
Title=CMS, 13 TeV, $p_{\mathrm{T}}^{\gamma}$ cross section
XLabel=$p_{\mathrm{T}}^{\gamma}$ (GeV)
YLabel=$\Delta\sigma/\Delta p_{\mathrm{T}}^{\gamma}$
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d08-x01-y01
Title=CMS, 13 TeV, $\eta^{\gamma}$ cross section
XLabel=$\eta^{\gamma}$
YLabel=$\Delta\sigma/\Delta \eta^{\gamma}$
LogY=0
YMax=1500
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d15-x01-y01
Title=CMS, 13 TeV, $\Delta R(\ell,\gamma)$ cross section
XLabel=$\Delta R(\ell,\gamma)$
YLabel=$\Delta\sigma/\Delta (\Delta R(\ell,\gamma))$
LogY=0
YMax=3000
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d22-x01-y01
Title=CMS, 13 TeV, $\Delta\eta(\ell,\gamma)$ cross section
XLabel=$\Delta\eta(\ell,\gamma)$
YLabel=$\Delta\sigma/\Delta (\Delta\eta(\ell,\gamma))$
LogY=0
YMax=1500
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d29-x01-y01
Title=CMS, 13 TeV, $m_{\mathrm{T}}^{\mathrm{cluster}}$ cross section
XLabel=$m_{\mathrm{T}}^{\mathrm{cluster}}$ (GeV)
YLabel=$\Delta\sigma/\Delta m_{\mathrm{T}}^{\mathrm{cluster}}$
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d36-x01-y01
Title=CMS, 13 TeV, $N_{\mathrm{jet}}$ cross section
XLabel=$N_{\mathrm{jet}}$
YLabel=$\sigma$
LogY=0
YMax=2500
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d40-x01-y01
Title=CMS, 13 TeV, $\Delta\eta(\ell,\gamma)$ cross section (RAZ)
XLabel=$\Delta\eta(\ell,\gamma)$
YLabel=$\Delta\sigma/\Delta (\Delta\eta(\ell,\gamma))$
LogY=0
YMax=300
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d54-x01-y01
Title=CMS, 13 TeV, $p_{\mathrm{T}}^{\gamma} \times |\phi_{f}|$ $(0,\pi/6)$
XLabel=$p_{\mathrm{T}}^{\gamma}$ (GeV)
YLabel=$\Delta^{2}\sigma/\Delta p_{\mathrm{T}}^{\gamma}\Delta|\phi_{f}|$
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d55-x01-y01
Title=CMS, 13 TeV, $p_{\mathrm{T}}^{\gamma} \times |\phi_{f}|$ $(\pi/6,\pi/3)$
XLabel=$p_{\mathrm{T}}^{\gamma}$ (GeV)
YLabel=$\Delta^{2}\sigma/\Delta p_{\mathrm{T}}^{\gamma}\Delta|\phi_{f}|$
END PLOT

BEGIN PLOT /CMS_2021_I1978840/d56-x01-y01
Title=CMS, 13 TeV, $p_{\mathrm{T}}^{\gamma} \times |\phi_{f}|$ $(\pi/3,\pi/2)$
XLabel=$p_{\mathrm{T}}^{\gamma}$ (GeV)
YLabel=$\Delta^{2}\sigma/\Delta p_{\mathrm{T}}^{\gamma}\Delta|\phi_{f}|$
END PLOT
