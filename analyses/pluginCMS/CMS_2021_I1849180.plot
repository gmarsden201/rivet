BEGIN PLOT /CMS_2021_I1849180/d01-x01-y01
Title=CMS, 8.16 TeV, fiducial cross section
XMin=15
XLabel=$m_{\mu\mu}$
YLabel=$d\sigma/dm_{\mu\mu}$
LogX=1
YMax=20
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d02-x01-y01
Title=CMS, 8.16 TeV, $15 < m_{\mu\mu} < 60$, fiducial cross section
XLabel=$y$
YLabel=$d\sigma/dy$
LogY=0
YMin=0
YMax=10
LogX=0
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d03-x01-y01
Title=CMS, 8.16 TeV, $60 < m_{\mu\mu} < 120$, fiducial cross section
XLabel=$y$
YLabel=$d\sigma/dy$
LogX=0
LogY=0
Ymin=0
YMax=52
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d04-x01-y01
Title=CMS, 8.16 TeV, $15 < m_{\mu\mu} < 60$, fiducial cross section
XMin=0.001
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LogX=1
XMin=1
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d05-x01-y01
Title=CMS, 8.16 TeV, $60 < m_{\mu\mu} < 120$, fiducial cross section
XMin=0.001
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LogX=1
XMin=1
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d06-x01-y01
Title=CMS, 8.16 TeV, $15 < m_{\mu\mu} < 60$, fiducial cross section
XMin=0.001
XLabel=$\phi^*$
YLabel=$d\sigma/d\phi^*$
LogX=1
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d07-x01-y01
Title=CMS, 8.16 TeV, $60 < m_{\mu\mu} < 120$, fiducial cross section
XMin=0.001
XLabel=$\phi^*$
YLabel=$d\sigma/d\phi^*$
LogX=1
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d09-x01-y01
Title=CMS, 8.16 TeV, full cross section
XMin=15
XLabel=$m_{\mu\mu}$
YLabel=$d\sigma/dm_{\mu\mu}$
LogX=1
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d10-x01-y01
Title=CMS, 8.16 TeV, $15 < m_{\mu\mu} < 60$, full cross section
XLabel=$y$
YLabel=$d\sigma/dy$
LogY=0
YMin=20
YMax=72
LogX=0
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d11-x01-y01
Title=CMS, 8.16 TeV, $60 < m_{\mu\mu} < 120$, full cross section
XLabel=$y$
YLabel=$d\sigma/dy$
LogX=0
LogY=0
YMin=20
YMax=72
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d12-x01-y01
Title=CMS, 8.16 TeV, $15 < m_{\mu\mu} < 60$, full cross section
XMin=0.001
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LogX=1
XMin=1
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d13-x01-y01
Title=CMS, 8.16 TeV, $60 < m_{\mu\mu} < 120$, full cross section
XMin=0.001
XLabel=$p_T$
YLabel=$d\sigma/dp_T$
LogX=1
XMin=1
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d14-x01-y01
Title=CMS, 8.16 TeV, $15 < m_{\mu\mu} < 60$, full cross section
XMin=0.001
XLabel=$\phi^*$
YLabel=$d\sigma/d\phi^*$
LogX=1
END PLOT

BEGIN PLOT /CMS_2021_I1849180/d15-x01-y01
Title=CMS, 8.16 TeV, $60 < m_{\mu\mu} < 120$, full cross section
XMin=0.001
XLabel=$\phi^*$
YLabel=$d\sigma/d\phi^*$
LogX=1
END PLOT
